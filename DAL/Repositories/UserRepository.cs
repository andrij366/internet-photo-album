﻿using DAL.Interfaces;
using solidWebApi;
using solidWebApi.DataModel;

namespace DAL.Repositories
{
    class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(MyDbContext context) : base(context)
        {
        }
    }
}
