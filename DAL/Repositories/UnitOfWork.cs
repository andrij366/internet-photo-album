﻿using DAL.Interfaces;
using DAL.Repositories;
using solidWebApi;
using System;
using System.Threading.Tasks;

namespace DAL
{
    class UnitOfWork
    {
        private IUserRepository _userRepository;
        private IPhotoRepository _photoRepository;
        private readonly MyDbContext _dbContext;

        public UnitOfWork(MyDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public IUserRepository UserRepository => _userRepository = _userRepository ?? new UserRepository(_dbContext);

        public IPhotoRepository PhotoRepository => _photoRepository = _photoRepository ?? new PhotoRepository(_dbContext);

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public async Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
