﻿using System.Collections.Generic;

namespace solidWebApi.DataModel
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual ICollection<Photo> Photos { get; set; } = new List<Photo>();
    }
}
