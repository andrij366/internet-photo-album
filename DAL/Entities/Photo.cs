﻿using System;

namespace solidWebApi.DataModel
{
    public class Photo : BaseEntity
    {
        public int UserId { get; set; }
        public byte[] Content { get; set; }
        public string Title { get; set; }
        public DateTime CreationDate { get; set; }
        public virtual User User { get; set; }
    }
}
