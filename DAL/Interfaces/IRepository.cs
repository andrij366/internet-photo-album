﻿using solidWebApi.DataModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        Task<IEnumerable<TEntity>> GetAllAsync();

        Task<TEntity> GetByIdAsync(int id);

        Task UpdateAsync(TEntity entity);

        Task DeleteByIdAsync(int id);

        Task<TEntity> CreateAsync(TEntity entity);
    }
}
