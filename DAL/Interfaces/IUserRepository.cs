﻿using solidWebApi.DataModel;

namespace DAL.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {

    }
}
